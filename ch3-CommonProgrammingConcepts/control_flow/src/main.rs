//// CONTROL FLOW

fn main() {
    let number = 3;

    // IF EXPRESSIONS
    // * condition must evaluate to a bool; no "truthy" or "falsy"
    // * can be used in variable assignment
    //   - all arms must evaluate to the same type

    if number < 5 {
        println!("condition was true!");
    } else {
        println!("condition was false!");
    }
    
    // LOOPS - as you would expect
    // WHILE LOOPS - same
    // FOR LOOPS - specifically for collections
    let a = [10, 20, 30, 40, 50];

    for element in a.iter() {
        println!("the value is: {}", element);
    }

    // another example using a reversed range
    for number in (1..4).rev() {
        println!("{}!", number);
    }
    println!("LIFTOFF!!!");
}
