fn main() {
    // SCALAR TYPES
    // represent single value
    // * integers
    // * floating-point numbers
    // * booleans
    // * characters

    // INTEGER TYPES
    // organized by length, signed or unsigned
    //
    // | length | signed | unsigned |
    // |----------------------------|
    // |  8-bit |  i8    |  u8      |
    // | 16-bit |  i16   |  u16     |
    // | 32-bit |  i32   |  u32     |
    // | 64-bit |  i64   |  u64     |
    // |  arch  |  isize |  usize   |  <- depends on runtime env arch (32/64)
    // |________|________|__________|
    //
    // Integer literals:
    // Decimal: 98_222
    // Hex:     0xff
    // Octal:   0x77
    // Binary:  0b1111_0000
    // Byte:    b'A'        <-- (u8 only)
    
    // FLOATING-POINT TYPES
    // 2 types: f32 & f64. Comparable speed on 64-bit systems, f64 grants
    // more precision. f64 default type for floats. Best even in 32-bit
    // systems unless benchmarking shows a significant speed issue
    
    let x = 2.0; // f64
    let y: f32 = 3.0; // f32

    // NUMERIC OPERATIONS
    
    // addition
    let sum = 5 + 10;

    // subtraction
    let difference = 95.5 - 4.3;

    // multiplication
    let product = 4 * 30;

    // division
    let quotient = 56.7 / 32.2;

    // remainder
    let remainder = 43 % 5;

    // BOOLEAN TYPE
    let t = true;
    let f: bool = false; // with explicit type annotation

    // CHARACTER TYPE
    // represents a Unicode Scalar Value:
    // U+0000 to U+D7FF && U+E000 to U+10FFFF inclusive
    let c = 'z';
    let z = 'ℤ';
    let heart_eyed_cat = '😻';

    //// COMPOUND TYPES
    // TUPLES
    // grouping of values with a variety of types
    let tup: (i32, f64, u8) = (500, 6.4, 1);
    // they can be destructured through pattern matching:
    let (x, y, z) = tup;
    println!("The value of y is: {}", y); // -> 6.4
    // their elements can be accessed by their indeces:
    let five_hundred = tup.0;
    let six_point_four = tup.1;
    let one = tup.2;

    // ARRAYS
    // * all elements must be of the same type
    // * length of array is fixed
    // * useful when you want data allocated
    //   on the STACK rather than the HEAP (Ch. 4)
    // * useful when you want to constrain # of elements
    // * alternate data type is VECTOR, which is allowed to
    //   grow/shrink (Ch. 8)
    let a = [1, 2, 3, 4, 5];
    let months = ["January", "February", "March", "April",
                  "May", "June", "July", "August",
                  "September", "October", "November", "December"];
    // Elements are indexed through bracket notation:
    let first = a[0];
    let second = a[1];
    // Attempting to access an element past the end of an array
    // will cause a runtime error:
    //
    // let element = a[10];
    // println!("The value of element is: {}", element); -> PANIC!
    // zomg you guys a runtime error is called Panic.
}
