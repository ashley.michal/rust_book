fn main() {
    let freezing = f_to_c(32); // -> 0
    println!("The freezing point of water is: {} degrees C", freezing);
    
    for i in 1..11 {
        let element = nth_fibonacci(i);
        println!("{}: {}", i, element);
    }
    days_of_xmas();
}

fn f_to_c(deg_f: i32) -> i32 {
    (deg_f - 32) * (5 / 9)
}


// 1,1,2,3,5,8,13,21,34,55...
fn nth_fibonacci(n: i32) -> i32 {
    if n == 1 || n == 2 {
        1
    } else {
        nth_fibonacci(n - 1) + nth_fibonacci(n-2)
    }
}

fn days_of_xmas() {
    let ordinals = [
        "first", "second", "third", "fourth", "fifth", "sixth",
        "seventh", "eighth", "nineth", "tenth", "eleventh", "twelfth",
    ];

    for n in 1..13 {
        println!(
            "On the {} day of Christmas my true love gave to me:",
            ordinals[n-1]
        );

        if n == 12 { println!("Twelve drummers drumming"); }
        if n >= 11 { println!("Eleven pipers piping"); }
        if n >= 10 { println!("Ten lords a-leaping"); }
        if n >=  9 { println!("Nine ladies dancing"); }
        if n >=  8 { println!("Eight maids a-milking"); }
        if n >=  7 { println!("Seven swans a-swimming"); }
        if n >=  6 { println!("Six geese a-laying"); }
        if n >=  5 { println!("FIIIIIIVE GOOOOOLD RIIIIINGS!"); }
        if n >=  4 { println!("Four calling birds"); }
        if n >=  3 { println!("Three french hens"); }
        if n >=  2 { println!("Two turtle doves"); }
        if n >=  1 { println!("A partridge in a pear tree"); }
        
        println!();
    }
}
