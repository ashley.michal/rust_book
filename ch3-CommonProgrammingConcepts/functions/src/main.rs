// function parameters require type declarations
fn another_function(x: i32, y: i32) {
    println!("The value of x is: {}", x);
    println!("The value of y is: {}", y);
}

// STATEMENTS AND EXPRESSIONS:
// * A statement is an instruction that performs an action
//   - examples: variable and function declarations
//   - statments end in semicolons, do not return a value
// * An expression evaluations to a resulting value
//   - examples: function calls, macro calls, blocks
//   - weirdness: expressions DO NOT end in semicolons.
// * declare return value types with `->`

fn five() -> i32 {
    5
}

fn plus_one(x: i32) -> i32 {
    // if you place a semicolon after the expression, the function
    // will not return a value -> mismatched types error
    x + 1
}

fn main() {
    another_function(5, 6);
    
    let x = five();
    println!("The value of x is: {}", x);

    let x = plus_one(5);
    println!("The value of x is: {}", x);
}

