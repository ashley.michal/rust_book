fn main() {

    // VARIABLES
    // * Variables are immutable by default
    // * Use the `mut` keyword to declare a mutable variable

    let mut x = 5;
    println!("The value of x is: {}", x); // -> 5
    // omiting the `mut` keyword will cause a compiler error here
    x = 6;
    println!("The value of x is: {}", x); // -> 6

    // CONSTANTS
    // * cannot be made mutable
    // * MUST be type-annotated
    // * may be declared in any scope, including global scope
    // * may only be set to a constant expression, not the result
    //   of a runtime computation

    // const MAX_POINTS: u32 = 100_000;

    // SHADOWING
    // * use the `let` keyword to "shadow" a variable name

    let x = 5;
    let x = x + 1;
    let x = x * 2;

    println!("The value of x is: {}", x); // => 12

    // shadowing does not mutate the original value, but reassigning
    // the variable name to a new value. This means that the new value
    // does not have to match the type of the old value:
    
    let spaces = "   ";
    let spaces = spaces.len();

    // mutable variables, on the other hand, must match the type of
    // the original value when mutated:

    // let mut spaces = "   ";
    // spaces = spaces.len(); -> compiler error "mismatched types"
}
