fn main() {
    // VARIABLE SCOPE
    // variables are valid from the point they are declared until the end
    // of the current scope. (generally denoted by closing curly braces
    if true {               // contrived scope; s is not valid here
        let s = "hello";    // s is valid from this point forward

        // do stuff with s
    }                       // s no longer valid after scope over

    // String types are meant to support mutable, growable text, or often
    // text whose size is unknown at compile time (think reading from the
    // file system. For this reason, they are stored on the heap. 
    //
    // String literals (ex `s = "hello"`) are known entities at compile time
    // and so are stored on the stack. For this reason they are immutable,
    // and therefore fast and efficient.

    let mut x = String::from("hello");
    x.push_str(", world!"); // push_str() appends a literal to a String
    println!("{}", x);      // this will print `hello, world!`

    // MOVE: 
    // stack values copy:
    let x = 5;
    let y = x;
    println!("x: {}, y: {}", x, y);

    // heap values move the pointer; previous variable invalidated:
    let s1 = String::from("hello");
    let s2 = s1;
    // println!("s1: {}", s1); -> error "use of moved value: `s1`
    //  Rust will never automatically create "deep" copies of heap
    //  data; automatic copying can be assumed to be inexpensive
    
    // CLONE:
    // used in cases where you wish to retain both variables:
    let s1 = String::from("hello");
    let s2 = s1.clone();
    println!("s1: {}, s2: {}", s1, s2);

    // Copy/Drop annotation: you can place the "Copy" trait on types
    // like integers which are stored on the stack. You cannot annotate
    // a type with "Copy" if it has implemented "Drop" and vice versa.
    
    // FUNCTIONS:
    // Passing a variable to a function will move or copy the way
    // assignment does:
    let s = String::from("hello");  // s comes into scope.

    takes_ownership(s);             // s's value moves into the function..
                                    // ... and so is no longer valid here.
    let x = 5;                      // x comes into scope.
    makes_copy(x);                  // x would move into the function, but
                                    // i32 is copy, so it's ok to still
                                    // use x afterward

    // return values
    let s1 = gives_ownership();         // gives ownership moves its
                                        // return value into s1.
    let s2 = String::from("hello");     // s2 comes into scope.
    let s3 = takes_and_gives_back(s2);  // s2 is moved into the fn,
                                        // which also moves its return
                                        // value into s3.
                                        
    // passing ownership can be tedious; this sequence requires us to
    // pass s1 into the function and then back out again by assignment
    // to a tuple. next chapter: References and borrowing.
    let s1 = String::from("hello");
    let (s2, len) = calculate_length(s1);
    println!("The length of '{}' is {}.", s2, len);
}

fn takes_ownership(some_string: String) { // some_string comes into scope.
    println!("{}", some_string);
}   // Here, some_string goes out of scope and `drop` is called. The
    // backing memory is freed.

fn makes_copy(some_integer: i32) {  // some_integer comes into scope.
    println!("{}", some_integer);
}   // Here, some_integer goes out of scope. Nothing special happens.

fn gives_ownership() -> String {
    let some_string = String::from("hello");
    some_string
}

fn takes_and_gives_back(a_string: String) -> String {
    a_string
}

fn calculate_length(s: String) -> (String, usize) {
    let length = s.len();
    (s, length)
}

