// TL;DR -
// 1. At any given time, you can have either but not both of:
//  * One mutable reference
//  * Any number of immutable references
// 2. References must always be valid

// Using references as function parameters is called BORROWING
fn main() {
    let s1 = String::from("hello");
    let len = calculate_length(&s1); // the & denotes a reference to s1
    println!("The length of '{}' is {}.", s1, len);

    // MUTABLE REFERENCES
    // in order to allow a function to change a referenced value,
    //  * the variable must be mutable, and
    //  * the reference must be explicitly declared `mut`
    let mut s = String::from("hello");
    change(&mut s);
    // note: you can only have one mutable reference to value in a
    // given scope. the following code will fail:
    // let r1 = &mut s;
    // let r2 = &mut s;
    //  -> cannot borrow `s` as a mutable more than once at a time
    //
    // Reasoning for this restriction is to prevent data races
    // at compile time.
    // DATA RACE: race condition in which:
    //  * two or more pointers access the same data at the same time
    //  * at least one of the pointers is used to write to data
    //  * no mechanism being used to synchronize access to data
    // if you must assign two references, create a new scope 
    // w/ curly braces:
    {
        let r1 = &mut s;
    }   // r1 goes out of scope here, so we can make a new reference.
    let r2 = &mut s;

    // you also cannot combine mutable and immutable references:
    // let r3 = &s;  -> cannot borrow `s` as mutable because it is also
    //                  borrowed as immutable
}

// ex: s is a reference to a string that refers to the value of s but does
// not own it and therefore will not drop the value when it goes out of scope.
fn calculate_length(s: &String) -> usize {
    s.len()
}   // Here, s goes out of scope, but because it does not have ownership of
    // the value it refers to, nothing happens.

// Note: borrowed values cannot be modified:
// the following function would throw a compiler error:
//      -> "cannot borrow immutable borrowed content as mutable"
// fn change(some_string: &String) {
//     some_string.push_str(", world");
// }

// you can get around this by declaring the reference as mutable
fn change(some_string: &mut String) {
    some_string.push_str(", world");
}

// DANGLING REFERENCES
// Rust's compiler guarantees that data does not go out of scope
// before the reference to the data does
// fn dangle() -> &String {
//     let s = String::from("hello");
// 
//     &s
// } // s goes out of scope and its reference would point to nothing.
// COMPILER ERROR:
// -> missing lifetime specifier
// help: this function's return type contains a borrowed value, but
// there is no value for it to be borrowed from.
//
// Alleviate this issue by returning the value:
fn no_dangle() -> String {
    let s = String::from("hello");
    s
}
