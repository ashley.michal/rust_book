// SLICES
// this approach is obnoxious bc the index can go out of phase with
// the string it is referencing. slices to the rescue!
fn first_word(s: &String) -> usize {
    let bytes = s.as_bytes(); // converts s to array of bytes

    // create iterator (returns each element in collection)
    // enumerate() wraps result in tuple: (index, &element)
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return i;
        }
    }
    
    s.len()
}

// instead of this:
// fn better_first_word(s: &String) -> &str {
// use a slice for a more consistent API since you can pass a slice
// directly, or easily take a slice of a String or string literal
fn better_first_word(s: &str) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..]
}

fn main() {
    let s = String::from("hello world");
    let hello = &s[0..5];
    let world = &s[6..11]; // stores ptr to starting position & len

    // similarly you can store a slice of an array:
    let a = [1, 2, 3, 4, 5];
    let slice = &a[1..3]; // has type &[i32]
}
