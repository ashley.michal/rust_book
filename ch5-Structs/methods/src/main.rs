// METHODS
// Methods can be defined on structs, enums, and traits
// always take `self` as their first parameter
// (instance methods, not class)

// copying struct definition from previous chapter
#[derive(Debug)]
struct Rectangle {
    length: u32,
    width: u32,
}

// impl: implementation block defines within context of named struct
// moved original function definition here, changed rectangle to
// ref to self; no need to declare argument types because Rust knows
// the type of self is Rectangle due to being in impl block.
// methods can treat `self` like any other parameter, that is they can
// take ownership, borrow immutably (as below), or borrow mutably.
impl Rectangle {
    fn area(&self) -> u32 {
        self.length * self.width
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.length > other.length && self.width > other.width
    }

    // ASSOCIATED FUNCTIONS: defined in impl block but do not act
    // on instances of entity so do not take self as parameter.
    // often used as constructors, e.g. String::from(arg1)

    fn square(size: u32) -> Rectangle {
        Rectangle { length: size, width: size }
    }
}

fn main() {
    let rect1 = Rectangle { length: 50, width: 30 };
    let rect2 = Rectangle { length: 40, width: 10 };
    let rect3 = Rectangle { length: 45, width: 60 };
    
    println!(
        "The area of the rectangle is {} square pixels.",
        rect1.area()
    );

    println!("Can rect1 hold rect2? {}", rect1.can_hold(&rect2));
    println!("Can rect1 hold rect3? {}", rect1.can_hold(&rect3));

    // associated functions are called using ::, similar to modules
    let square = Rectangle::square(25);
    println!(
        "The area of the square is {} square pixels.",
        square.area()
    );
}
