// The following struct owns all of its data, so uses the String type
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

// it is possible for structs to store references to data owned by
// something else, but to do so requires the use of LIFETIMES (see
// ch. 10). If you try to store a reference without specifying
// lifetimes, you'll get a compiler error "missing lifetime specifier".
// struct RefUser {
//     username: &str,
//     email: &str,
//     sign_in_count: u64,
//     active: bool,
// }

fn main() {
    let user1 = User {
        email: String::from("something@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };

    // access struct values with dot notation
    println!("user1 email: {}", user1.email);

    let length1 = 50;
    let width1 = 30;

    println!(
        "The area of the rectangle is {} square pixels.",
        area(length1, width1)
    );

    let rect1 = (length1, width1);

    println!(
        "The area of the rectangle is {} square pixels.",
        tuple_area(rect1)
    );

    let rect2 = Rectangle { length: length1, width: width1 };

    println!(
        "The area of the rectangle is {} square pixels.",
        struct_area(&rect2)
    );

    // macro `println!` with {} uses formatting known as `Display`
    // with {:?} uses formatting called `Debug`
    // both of these are TRAITS that our struct must opt-in to
    println!("rect2 is {:?}", rect2); // -> "{ length: 50, width: 30 }"
    println!("rect2 is {:#?}", rect2); // pretty-print
        // {
        //    length: 50,
        //    width: 30
        // }
}

fn area(length: u32, width: u32) -> u32 {
    length * width
}

fn tuple_area(dimensions: (u32, u32)) -> u32 {
    dimensions.0 * dimensions.1
}

// Rust provides fn'ty to print debugging info; must opt-in explicitly:
#[derive(Debug)]
struct Rectangle {
    length: u32,
    width: u32,
}

fn struct_area(rectangle: &Rectangle) -> u32 {
    rectangle.length * rectangle.width
}

