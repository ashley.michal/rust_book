// ENUMS
// similar to algebraic data types
// an IP address can be one of two types: version 4 or version 6.
// in both cases it should be treated like an IP address.
// The following enum expresses that:
// in this case, V4 and V6 are VARIANTS of IP addresses
enum IpAddrKind {
    V4,
    V6,
}

struct IpAddr {
    kind: IpAddrKind,
    address: String,
}

let home = IpAddr {
    kind: IpAddrKind::V4,
    address: String::from("127.0.0.1"),
};

let loopback = IpAddr {
    kind: IpAddrKind::V6,
    address: String::from("::1"),
};

// to be more concise...:

enum IpAddr {
    V4(u8, u8, u8, u8),
    V6(String),
}

let home = IpAddr::V4(127, 0, 0, 1);
let loopback = IpAddr::V6(String::from("::1"));

// but of course the standard lib has an IP address type definition!
struct Ipv4Addr { ... }

struct Ipv6Addr { ... }

// variants can be any type of data, including other enums
enum IpAddr {
    V4(Ipv4Addr),
    V6(Ipv6Addr),
}


// this expresses a single type, as opposed to writing the variants
// as structs, which would each have their own types
enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}

// enums can also have methods namespaced under them
impl Message {
    fn call(&self) {
        // method body defined here
    }
}

let m = Message::Write(String::from("hello"));
m.call();

// OPTION instead of Null values!
// encodes scenario where a value can be defined as something or 
// nothing. yeah! fuck null!
// the Option type is included in the prelude, don't need to import
// same with Some and None (don't need to prefix these)

enum Option<T> { // <T> indicates generic type
    Some(T), // Some can hold a single value of any type.
    None,
}

let some_number = Some(5);
let some_string = Some("a string");

// when using None, must declare type of Option
let absent_number: Option<i32> = None;

// useful because Option<T> and T are different types, compiler will
// therefore protect against type errors; i.e. you have to explicitly
// convert Option<T> to a T type before you can perform T operations.

// the following will throw an error:
let x: i8 = 5;
let y: Option<i8> = Some(5);

let sum = x + y; // cannot add i8 and Option<i8>

fn main() {
    // can create instances of enum variants:
    let four = IpAddrKind::V4;
    let six = IpAddrKind::V6;

    // can write functions that take any variant of our enum type:
    fn route(ip_type: IpAddrKind) { }

    // route can be called with either V4 or V6!
    route(IpAddrKind::V4);
    route(IpAddrKind::V6);
}
