PRIVACY:

1. If an item (function, module) is public, it can be accessed through any of its parent modules.
2. If an item is private, it may be accessed only by the current module and its child modules.
