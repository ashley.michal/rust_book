mod outermost {
    pub fn middle_function() {}
    
    pub fn middle_secret_function() {}

    pub mod inside {
        pub fn inner_function() {
            // starting with :: means start with root module, like /
            ::outermost::middle_secret_function();
        }

        pub fn secret_function() {}
    }
}

fn try_me() {
    outermost::middle_function();
    outermost::middle_secret_function();
    outermost::inside::inner_function();
    outermost::inside::secret_function();
}

// the USE keyword for concise imports:
pub mod a {
    pub mod series {
        pub mod of {
            pub fn nested_modules() {}
        }
    }
}

fn main() {
    // instead of this:
    a::series::of::nested_modules();

    // import with use:
    use a::series:of;
    of::nested_modules();
}

// use can also work with variants of an enum
// or it seems anything that can be namespaced
enum TrafficLight {
    Red,
    Yellow,
    Green,
}

use TrafficLight::{Red, Yellow};

fn main2() {
    let red = Red;
    let yellow = Yellow;
    let green = TrafficLight::Green; // because we didn't "use" it.
}

// can "glob" imports with *
use TrafficLight::* // imports all variants into scope. use sparingly

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
    }
}
