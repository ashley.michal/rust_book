use std::collections::HashMap;

pub fn mean(numbers: &[i32]) -> f32 {
    numbers.iter().sum::<i32>() as f32 / numbers.len() as f32
}

pub fn median(numbers: &mut [i32]) -> i32 {
    numbers.sort();
    let mid = numbers.len() / 2;
    numbers[mid]
}

pub fn mode(numbers: &[i32]) -> i32 {
    let mut occurrences = HashMap::new();
    for &value in numbers {
        *occurrences.entry(value).or_insert(0) += 1;
    }

    occurrences.into_iter()
        .max_by_key(|&(_, count)| count)
        .map(|(val, _)| val)
        .expect("Cannot compute the mode of zero numbers")
}
             

#[cfg(test)]
mod tests {
    #[test]
    fn test_mean() {
        let numbers = [42, 1, 36, 34, 76, 378, 43, 1, 43, 54, 2, 3, 43];

        assert_eq!(::mean(&numbers), 58.153847);
    }

    #[test]
    fn test_median() {
        let mut numbers = [42, 1, 36, 34, 76, 378, 43, 1, 43, 54, 2, 3, 43];

        assert_eq!(::median(&mut numbers), 42);
    }

    #[test]
    fn test_mode() {
        let numbers = [42, 1, 36, 34, 76, 378, 43, 1, 43, 54, 2, 3, 43];

        assert_eq!(::mode(&numbers), 43);
    }
}
