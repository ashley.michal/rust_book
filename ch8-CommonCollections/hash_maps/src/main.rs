// HASH MAPS
// provided by std library
// least often used of the 3 main collections (vector, string, hashmap)
// stored on the heap
// all keys must be of same type, all values must be of same type

fn main() {
    use std::collections::HashMap;

    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    // can execute `.collect` on vector of tuples where each tuple is a key/value pair
    let teams = vec![String::from("Blue"), String::from("Yellow")];
    let initial_scores = vec![10, 50];

    // must declare HashMap type bc collect is implemented on many interfaces
    // don't need to declare key/value types because they can be inferred.
    let scores: HashMap<_, _> = teams.iter().zip(initial_scores.iter()).collect();

    // OWNERSHIP
    // * if type implements `Copy` trait, values are copied.
    // * if type is owned, values will be moved, hash becomes owner.
    // * if references are inserted, they must be valid for at least the life of the hashmap
    let field_name = String::from("Favorite color");
    let field_value = String::from("Blue");

    let mut map = HashMap::new();
    map.insert(field_name, field_value);
    // field_name and field_value are now invalid.

    // ACCESSING VALUES
    // using `.get` returns an Option<V>
    let team_name = String::from("Blue");
    let score = scores.get(&team_name); // -> Some(10) 

    // iterating over keys
    for (key, value) in &scores {
        println!("{}: {}", key, value);
    } // prints in arbitrary order

    // UPDATING
    // each key can only have one value, so we have 3 choices:
    // 1. overwrite a value
    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Blue"), 25);

    println!("{:?}", scores);

    // 2. only insert if the key has no value:
    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);

    scores.entry(String::from("Yellow")).or_insert(50);
    scores.entry(String::from("Blue")).or_insert(50);

    println!("{:?}", scores); // -> {"Yellow": 50, "Blue": 10} 

    // 3. update value based on old value
    let text = "hello world wonderful world";

    let mut map = HashMap::new();

    for word in text.split_whitespace() {
        let count = map.entry(word).or_insert(0); // .or_insert returns mutable reference!
        *count += 1; // dereference count using * to allow assignment
    }

    println!("{:?}", map); // -> {"world: 2, "hello": 1, "wonderful": 1}
}
