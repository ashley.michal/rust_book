fn main() {
    // STRINGS

    // strings are collections of bytes
    // `String` is a growable, mutable, owned, UTF-8 encoded string type.
    // `str` is a string slice, a reference to some UTF-8 encoded string data
    // string literals are stored in the binary output of the program (neat!)

    // Creating a new empty string
    let s = String::new();

    // converting a datum to a string, available on any type that implements `Display` trait
    let data = "initial contents"; // a string literal, which implements `Display`
    let s = data.to_string();

    // or
    let s = String::from(data);

    // updating a string
    let mut s = String::from("foo");
    s.push_str("bar");

    // push_str takes a slice (remember literals are slices)
    let mut s1 = String::from("foo");
    let s2 = String::from("bar");
    s1.push_str(&s2);

    // push takes single character
    let mut s = String::from("lo");
    s.push('l');

    // Can concatenate strings using either + operator or format! macro
    let s1 = String::from("Hello, ");
    let s2 = String::from("world!");
    let s3 = s1 + &s2; // note that s1 has been moved to s3 and can no longer be used

    // plus operator uses add method, w/ approximate signature:
    // fn add(self, s: &str) -> String {
    // since `self` is not a reference, the add method takes ownership of s1, appends a copy of
    // s2, then returns ownership of the result, which above is assigned to s3.

    // formatting: works like `println!` except returns the String rather than outputting
    let s1 = String::from("Hello, ");
    let s2 = String::from("world!");
    let s = s1 + "-" + &s2 + "-" + &s3; // gets unwieldy
    
    let s1 = String::from("Hello, ");
    let s2 = String::from("world!");
    let s = format!("{}-{}-{}", s1, s2, s3); // nicer
    
    // indexing into strings: not really a thing...
    // really long explanation but mostly just use .chars and .bytes iterators

    println!("Hello, world!");
}
