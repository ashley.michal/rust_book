// VECTORS
// store collection of values in data structure
// puts all values adjacent in memory
// only store values of the same type


// CREATING
// must declare element type for empty vector
let v: Vec<i32> = Vec::new();

// more commonly will initialize with values, 
// so we can rely on type inference.
// Rust provides the `vec!` macro for convenience:
let v: vec![1, 2, 3];


// UPDATING
// to add elements, use the .push method:
let mut v = Vec::new();

v.push(5);
v.push(6);
v.push(7);
v.push(8);


// DELETING
// when a vector gets dropped (goes out of scope), so do its elements.
{
    let v = vec![1, 2, 3, 4];
} // <- v goes out of scope here and is freed

// READING
let v = vec![1, 2, 3, 4, 5];

// gives a reference to the third element
// if you try to access something that isn't there, Rust will panic!
let third: &i32 = &v[2]; 

// gives an Option<&T>
// if you try to access something that isn't there, it's cool you just get None.
let third: Option<&i32> = v.get(2); 

let does_not_exist = &v[100];       // -> panic!
let does_not_exist = v.get(100);    // -> Option<None>

// INVALID REFERENCES
// borrow checker enforces ownership:
let mut v = vec![1, 2, 3, 4, 5];
let first = &v[0];
v.push(6);
// the above gives a nasty error because we might have to move the vector
// if the `push` increases the size of the vector to greater than its current
// place in memory, which would make the variable `first` a dangling pointer.

// USING AN ENUM TO STORE MULTIPLE TYPES
// by creating a custom enum, you can encapsulate many primitive types into 
// the same "type" and store them in a vector.
enum SpreadsheetCell {
    Int(i32),
    Float(f64),
    Text(String),
}

let row = vec![
    SpreadsheetCell::Int(3),
    SpreadsheetCell::Text(String::from("blue")),
    SpreadsheetCell::Float(10.12),
];

fn main() {
    println!("Hello, world!");
}
